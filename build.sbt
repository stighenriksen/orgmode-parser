lazy val root = (project in file(".")).
  settings(
    name := "orgparser",
    version := "0.1.0",
    scalaVersion := "2.12.5"
  )

libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.3"

libraryDependencies += "com.lihaoyi" %% "fastparse" % "1.0.0"

scalaVersion in ThisBuild := "2.12.5"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0" % "test"

