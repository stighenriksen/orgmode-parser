package com.stighenriksen.orgparser
import fastparse.all._

import Lexical._

object Grammar {

  case class OrgDocument(maybeTopSection: Option[Section], headlines: Seq[Headline]) {

    def print(): String = ???
  }

  case class SectionLine(value: String)
  case class Section(sectionLines: Seq[String])
  
  lazy val orgDocument: Parser[OrgDocument] =
    newLine.? ~ headline.map(h => OrgDocument(None, headlines = Seq(h)))

  val newLine = P("\n").rep
  val spaces = P( (CharsWhileIn(" ") | "\n").rep )

  lazy val headline: Parser[Headline] =
    headlineWithTitle 

//  stars |
//  stars ~ keyword |
//  stars  ~ priority ~ title |
//  stars  ~ priority ~ title ~ tags

  sealed trait Headline {
    val stars: Stars
    val maybeSection: Option[Section]
    val nestedHeadlines: Seq[Headline]
  }

  lazy val emptyHeadline: Parser[EmptyHeadline] =
    stars.map(EmptyHeadline(_, None, Seq()))

  val Newline: Parser[Unit] = P( StringIn("\r\n", "\n"))

  lazy val headlineWithTitle = (stars ~ P(CharPred(c => c != '\n').rep.!)).map{ case (stars, string) => HeadlineWithTitle(stars, string, None, Seq())}

  val stars: Parser[Stars] =
    P("*".rep(1).!).map(starsStr => Stars(starsStr.length))

//    stars.map(EmptyHeadline(_, None, Seq()))

  case class EmptyHeadline(stars: Stars,
                           maybeSection: Option[Section],
                           nestedHeadlines: Seq[Headline])
      extends Headline

  case class HeadlineWithTitle(stars: Stars,
                               title: String,
                               maybeSection: Option[Section],
                               nestedHeadlines: Seq[Headline])
      extends Headline

//  case class HeadlineWithKeyword(stars: Stars, keyword: Keyword) extends Headline

  case class Stars(num: Int)
  case class Keyword(value: String)

  val keyword: Parser[Option[Keyword]] = P(
    StringIn(KnownKeywords: _*).!.map(s => Keyword(s)).?)

  val priority: Parser[String] = P("[" ~ "#" ~ CharIn('a' to 'z').! ~ "]")

  val title: Parser[String] = P(CharsWhile(_ != ' ').!)


  // TODO problem: how

  lazy val section: Parser[String] = ???

  val alphaNumericChars: IndexedSeq[Char] = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')

  val tagChars = alphaNumericChars :+ '_' :+ '@' :+ '#'

  val tags: Parser[String] = P(":" ~ CharIn(tagChars).rep.!  ~ ":")
  val orgHeadline = stars ~ keyword ~ priority ~ title ~ tags

  lazy val KnownKeywords = Seq("TODO", "DONE")

}
