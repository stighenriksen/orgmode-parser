package com.stighenriksen.orgparser
import java.io.File
import scala.io.Source
import fastparse.all._
import Grammar._

object Parser  {

  def parse(string: String): Parsed[OrgDocument] = {
    orgDocument.parse(string)
  }

}


