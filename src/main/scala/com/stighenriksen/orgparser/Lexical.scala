package com.stighenriksen.orgparser

import fastparse.all._

object Lexical {

  val title: Parser[String] = P( CharsWhile(_ != ' ').!)
  val keyword: Parser[Option[Keyword]] = P(StringIn(knownKeywords: _*).!.map(s => Keyword(s)).?)
  val priority: Parser[String] = P("[" ~ "#" ~  CharIn('a' to 'z').! ~ "]")
  val stars: Parser[Stars] = P("*".rep(1).!).map(starsStr => Stars(starsStr.length))

  private val alphaNumericChars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
  private val tagChars = alphaNumericChars :+ '_' :+ '@' :+ '#'
  val tags: Parser[Seq[String]] = P(":" ~ CharIn(tagChars).rep.! ~ ":").rep

  def knownKeywords = Seq("TODO", "DONE")

  case class Stars(num: Int)
  case class Keyword(value: String)

}
