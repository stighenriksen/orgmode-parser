package com.stighenriksen.orgparser

import org.scalatest.{FlatSpec, FunSuite, Matchers}
import fastparse.all._

import java.io.File

import Grammar._

class ParserTest extends FlatSpec with Matchers {

  "Parser" should "parse org doc with one empty headline with one star" in {

    val result = Parser.parse("*")

    result.isInstanceOf[Parsed.Success[_]] shouldBe true
    val orgDocument: OrgDocument = result.get.value

    orgDocument.headlines(0)
    orgDocument.headlines(0).stars.num shouldEqual 1
  }

  "Parser" should "parse org doc with headline with title" in {

    val result = Parser.parse("* some title")

    result.isInstanceOf[Parsed.Success[_]] shouldBe true
    val orgDocument: OrgDocument = result.get.value

    println(orgDocument)
    println(orgDocument.headlines)
    println(orgDocument.headlines(0).getClass)

    val firstHeadline = orgDocument.headlines(0)
    firstHeadline shouldBe a[HeadlineWithTitle]

    firstHeadline.asInstanceOf[HeadlineWithTitle].title shouldEqual " some title"

    orgDocument.headlines(0).stars.num shouldEqual 1
  }

  "Parser" should "parse org doc with headline with title and then output same document back" in {

    val result = Parser.parse("* some title")

    result.isInstanceOf[Parsed.Success[_]] shouldBe true
    val orgDocument: OrgDocument = result.get.value

    println(orgDocument)
    println(orgDocument.headlines)
    println(orgDocument.headlines(0).getClass)

    val firstHeadline = orgDocument.headlines(0)
    firstHeadline shouldBe a[HeadlineWithTitle]

    firstHeadline.asInstanceOf[HeadlineWithTitle].title shouldEqual " some title"

    orgDocument.headlines(0).stars.num shouldEqual 1

    orgDocument.print() shouldEqual ("* some title")

  }

  "Parser" should "parse org doc with newlines first before headline with no title" in {

    val result = Parser.parse("""
   |
   |*
  """.stripMargin)

    println(result)
    result.isInstanceOf[Parsed.Success[_]] shouldBe true
    val orgDocument: OrgDocument = result.get.value
    orgDocument.headlines(0)
    orgDocument.headlines(0).stars.num shouldEqual 1
  }

}
